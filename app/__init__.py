from flask import Flask 
from pymongo import MongoClient

app = Flask("__name__")

# client = MongoClient('localhost', 27017, username='username', password='password')
# db = client.flask_db
# todos = db.todos

from app import routes
