FROM python:3.8

WORKDIR /

COPY requirements.txt .

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt


EXPOSE 6001

CMD ["python", "-u", "main.py"]
